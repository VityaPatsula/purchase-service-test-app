﻿namespace Purchase.Abstractions.Data
{
    // should it be async even if implementation is sync?
    // probably yes, I can change implementation to other, that can connects to db.
    public interface IRepository<T, K> where T : IEntity<K>
    {
        public void Add(T item);
        public bool TrySearch(K id, out T result);
        public IEnumerable<T> Filter(Predicate<T> filter, int skip, int take);
        public IEnumerable<T> GetAll(int skip, int take);
    }
}
