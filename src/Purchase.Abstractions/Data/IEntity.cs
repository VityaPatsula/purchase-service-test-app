﻿namespace Purchase.Abstractions.Data
{
    public interface IEntity<K> : IComparable<K>
    {
        public K Id { get; set; }
    }
}
