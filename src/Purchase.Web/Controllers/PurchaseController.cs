﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Purchase.Infrastructure.Mediator.Models.Requests;

namespace Purchase.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PurchaseController : ControllerBase
    {
        private readonly IMediator _mediator;
        public PurchaseController(IMediator mediator)
        {
            _mediator = mediator;
        }
        [HttpPost]
        public async Task<JsonResult> AddPurchase([FromBody] DTOs.Requests.AddPurchaseRequest dto)
        {
            // TODO: use AutoMapper
            var orders = dto.Orders.Select(order => new OrderRequest(order.Currency, order.Amount));
            var request = new AddPurchaseRequest(
                dto.FirstName,
                dto.DisplayName,
                dto.LastName,
                dto.PrimaryEmail,
                dto.SecondaryEmail,
                orders
                );
            var response = await _mediator.Send(request);

            var res = new DTOs.Responses.AddPurchaseResponse {
                PurchaseId = response.PurchaseId
            };
            return new JsonResult(res);
        }

        [HttpGet("{id}")]
        public async Task<JsonResult> TryGetPurchaseById([FromRoute] Guid id)
        {
            var response = await _mediator.Send(new TryGetPurchaseByIdRequest(id));

            var res = new DTOs.Responses.TryGetPurchaseByIdResponse
            {
                Success = response.Success,
                Purchase = new DTOs.Responses.PurchaseResponse
                {
                    Id = response.Purchase.Id,
                    DisplayName = response.Purchase.DisplayName,
                    FirstName = response.Purchase.FirstName,
                    LastName = response.Purchase.LastName,
                    PrimaryEmail = response.Purchase.PrimaryEmail,
                    SecondaryEmail = response.Purchase.SecondaryEmail,
                    OrdersIds = response.Purchase.OrdersIds
                }
            };
            return new JsonResult(res);
        }

        [HttpGet("Filter/{skip}/{take}")]
        public async Task<JsonResult> TryGetPurchaseByFilter(
            [FromRoute] int skip,
            [FromRoute] int take,
            [FromQuery] string? firstName,
            [FromQuery] string? displayName,
            [FromQuery] string? lastName,
            [FromQuery] string? primaryEmail,
            [FromQuery] string? secondaryEmail)
        {
            var request = new GetPurchaseByFilterRequest(skip, take, firstName, displayName, lastName, primaryEmail, secondaryEmail);
            var response = await _mediator.Send(request);
            return new JsonResult(MapPurchases(response.Purchase));
        }

        private IEnumerable<DTOs.Responses.PurchaseResponse> MapPurchases(IEnumerable<Core.Models.Purchase> purchases)
        {
            return purchases.Select(purchase => new DTOs.Responses.PurchaseResponse
            {
                Id = purchase.Id,
                DisplayName = purchase.DisplayName,
                FirstName = purchase.FirstName,
                LastName = purchase.LastName,
                PrimaryEmail = purchase.PrimaryEmail,
                SecondaryEmail = purchase.SecondaryEmail,
                OrdersIds = purchase.OrdersIds
            });
        }
    }
}
