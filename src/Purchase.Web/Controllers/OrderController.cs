﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Purchase.Core.Models;
using Purchase.Infrastructure.Mediator.Models.Requests;

namespace Purchase.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IMediator _mediator;
        public OrderController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("{purchaseId}")]
        public async Task<JsonResult> AddOrders([FromRoute] Guid purchaseId, [FromBody] IEnumerable<DTOs.Requests.OrderRequest> models)
        {
            var request = new AddOrderRequest(
                purchaseId,
                models.Select(order => new OrderRequest(order.Currency, order.Amount))
                );
            var orders = await _mediator.Send(request);

            var response = new DTOs.Responses.OrdersResponse
            {
                Orders = MapOrders(orders.Orders)
            };
            return new JsonResult(response);
        }

        [HttpGet]
        public async Task<JsonResult> GetOrdersByIds([FromQuery] IEnumerable<string> ordersIds)
        {
            var request = new GetOrderByIdRequest(ordersIds);
            var orders = await _mediator.Send(request);
            var response = new DTOs.Responses.OrdersResponse
            {
                Orders = MapOrders(orders.Orders)
            };
            return new JsonResult(response);
        }

        [HttpGet("Filter/{skip}/{take}")]
        public async Task<JsonResult> GetOrdersByFilter(
            [FromRoute] int skip,
            [FromRoute] int take,
            [FromQuery] DateTime? creationDateUTC,
            [FromQuery] string? currency,
            [FromQuery] int? amount)
        {
            var request = new GetOrdersByFilterRequest(skip, take, creationDateUTC, currency, amount);
            var response = await _mediator.Send(request);

            return new JsonResult(new DTOs.Responses.OrdersResponse { Orders = MapOrders(response.Orders) });
        }

        private IEnumerable<DTOs.Responses.OrderResponse> MapOrders(IEnumerable<Order> orders)
        {
            return orders.Select(o => new DTOs.Responses.OrderResponse
            {
                Id = o.Id,
                CreationDateUTC = o.CreationDate,
                Amount = o.Amount,
                Currency = o.Currency
            });
        }
    }
}
