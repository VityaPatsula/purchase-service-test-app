﻿using Purchase.Infrastructure.Exceptions;

namespace Purchase.Web.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }
        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (OrderNotFoundException e)
            {
                await ResponseNotFound(context, e.Message, e);
            }
            catch (PurchaseNotFoundException e)
            {
                await ResponseNotFound(context, e.Message, e);
            }
            catch (Exception e)
            {
                _logger.LogCritical(e.Message, e);
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            }
        }

        private async Task ResponseNotFound<T>(HttpContext context, string message, T exception) where T : Exception
        {
            _logger.LogError(message, exception);
            context.Response.StatusCode = StatusCodes.Status404NotFound;
            await context.Response.WriteAsJsonAsync(message);
        }
    }
}
