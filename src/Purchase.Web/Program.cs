using MediatR;
using Purchase.Abstractions.Data;
using Purchase.Core.Models;
using Purchase.Core.Tree;
using Purchase.Infrastructure.Data;
using Purchase.Infrastructure.Extensions;
using Purchase.Web.Middlewares;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddRouting();
builder.Services.AddControllers();
builder.Services.AddSingleton<BinaryTree<Order, string>>();
builder.Services.AddSingleton<BinaryTree<Purchase.Core.Models.Purchase, Guid>>();
builder.Services.AddTransient<IRepository<Order, string>, Repository<Order, string>>();
builder.Services.AddTransient<IRepository<Purchase.Core.Models.Purchase, Guid>, Repository<Purchase.Core.Models.Purchase, Guid>>();
builder.Services.AddMediatorLibrary();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseMiddleware<ExceptionMiddleware>();
app.UseHttpsRedirection();
app.UseRouting();
app.MapControllers();
app.Run();