﻿namespace Purchase.Web.DTOs.Responses
{
    public class PurchaseResponse
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string DisplayName { get; set; }
        public string LastName { get; set; }
        public string PrimaryEmail { get; set; }
        public string SecondaryEmail { get; set; }
        public List<string> OrdersIds { get; set; }
    }
}
