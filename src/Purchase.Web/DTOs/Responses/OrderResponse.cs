﻿namespace Purchase.Web.DTOs.Responses
{
    public class OrderResponse
    {
        public string Id { get; set; }
        public DateTime CreationDateUTC { get; set; } // UTC
        public string Currency { get; set; }
        public int Amount { get; set; }
    }
}
