﻿namespace Purchase.Web.DTOs.Responses
{
    public class OrdersResponse
    {
        public IEnumerable<OrderResponse> Orders { get; set; }
    }
}
