﻿namespace Purchase.Web.DTOs.Responses
{
    public class AddPurchaseResponse
    {
        public Guid PurchaseId { get; set; }
    }
}
