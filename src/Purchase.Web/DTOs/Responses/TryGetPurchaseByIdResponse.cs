﻿namespace Purchase.Web.DTOs.Responses
{
    public class TryGetPurchaseByIdResponse
    {
        public bool Success { get; set; }
        public PurchaseResponse Purchase { get; set; }

    }
}
