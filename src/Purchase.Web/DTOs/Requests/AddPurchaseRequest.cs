﻿namespace Purchase.Web.DTOs.Requests
{
    public class AddPurchaseRequest
    {
        public string FirstName { get; set; }
        public string DisplayName { get; set; }
        public string LastName { get; set; }
        public string PrimaryEmail { get; set; }
        public string SecondaryEmail { get; set; }
        public IEnumerable<OrderRequest> Orders { get; set; }
    }
}
