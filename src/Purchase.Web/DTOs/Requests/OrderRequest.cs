﻿namespace Purchase.Web.DTOs.Requests
{
    public class OrderRequest
    {
        public string Currency { get; set; }
        public int Amount { get; set; }
    }
}
