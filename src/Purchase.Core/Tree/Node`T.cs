﻿using Purchase.Abstractions.Data;

namespace Purchase.Core.Tree
{
    //TODO:
    // - implement red-black tree

    // I do not use IComparer because tree is sorted by one certain way. It will not find needed element if explicit IComparer use other way to compare.
    // This tree can contain duplicates
    // This is not a balanced tree, so the worst case is O(N)
    public class Node<T, K> where T : IEntity<K>
    {
        #region Diagnostics 
        private static int _id = 0;
        public Node()
        {
            Id = Interlocked.Increment(ref _id);
        }
        public int Id { get; private set; }
        #endregion
        public T Key { get; set; } // TODO: make readonly
        public Node<T, K>? Parent { get; set; } // Do I need this?
        public Node<T, K>? Left { get; set; }
        public Node<T, K>? Right { get; set; }


    }
}
