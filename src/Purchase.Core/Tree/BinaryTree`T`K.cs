﻿using Purchase.Abstractions.Data;

namespace Purchase.Core.Tree
{
    public class BinaryTree<T, K> where T : IEntity<K>
    {
        private readonly Node<T, K> _root;
        private readonly object _lock;
        public BinaryTree()
        {
            _root = new Node<T, K>();// with key = null
            _lock = new object();
        }

        public void Insert(T key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key)); // todo: add args
            }
            lock (_lock)
            {
                InsertInternal(key);
            }
        }

        public bool TrySearch(K id, out T result)
        {
            if (id == null)
            {
                throw new ArgumentNullException(nameof(id));
            }

            T temp = default;
            bool suscess = false;
            lock (_lock)
            {
                suscess = TrySearchInternal(id, out temp);
            }
            result = temp;
            return suscess;
        }

        public List<T> Filter(Predicate<T> filter, int skip = 0, int take = 100)
        {
            List<T> result = null;
            lock (_lock)
            {
                result = FilterInternal(filter, skip, take);
            }
            return result;
        }

        private void InsertInternal(T key)
        {
            if (_root.Key == null) // if tree is empty
            {
                _root.Key = key;
                return;
            }

            Node<T, K> y = null; // pre-last node
            var x = _root; // last node
            while (x != null)
            {// search for suitable parent node for a newNode
                y = x;
                if (key.CompareTo(x.Key.Id) <= 0) // if new element less or equal x
                {
                    x = x.Left;
                }
                else
                {
                    x = x.Right;
                }
            }

            //add new node to y node
            var newNode = new Node<T, K>
            {
                Key = key,
                Parent = y,
            };
            if (newNode.Key.CompareTo(y.Key.Id) <= 0)
            {
                y.Left = newNode;
            }
            else
            {
                y.Right = newNode;
            }
        }
        /// <summary>
        /// Returns the first element that match T key
        /// </summary>
        /// <param name="key"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentNullException"></exception>
        private bool TrySearchInternal(K id, out T result)
        {
            if (_root.Key == null)
            {
                result = default;
                return false;
            }
            var x = _root;
            while (x != null)
            {
                var compare = x.Key.CompareTo(id);
                switch (compare)
                {
                    case 0: // are equals
                        {
                            result = x.Key;
                            return true;
                        }
                    case -1: // x node lower than id
                        {
                            x = x.Right;
                            break;
                        }
                    case 1: // x node bigger than id
                        {
                            x = x.Left;
                            break;
                        }
                }
            }
            // here x is null, key not found
            result = default;
            return false;
        }

        private List<T> FilterInternal(Predicate<T> filter, int skip = 0, int take = 100)
        {
            var skipped = 0;
            var taken = 0;
            var stack = new Stack<Node<T, K>>(); // will stack be a problem with yield? For example with large amount of data.
            var result = new List<T>();

            var cursor = _root;
            while (cursor != null || stack.Count > 0)
            {
                while (cursor != null)
                {
                    stack.Push(cursor);
                    cursor = cursor.Left;
                }
                cursor = stack.Pop();

                // return filtered keys
                if (filter(cursor.Key))
                {
                    if (skipped >= skip && taken < take)
                    {
                        result.Add(cursor.Key);
                        taken++;
                        if (taken == take)
                        {
                            break;
                        }
                    }

                    skipped++;
                }

                cursor = cursor.Right;
            }
            return result;
        }
    }
}
