﻿using Purchase.Abstractions.Data;

namespace Purchase.Core.Models
{
    public class Order : IEntity<string>
    {
        public string Id { get; set; }
        public DateTime CreationDate { get; set; } // UTC
        public string Currency { get; set; }
        public int Amount { get; set; }

        public int CompareTo(string? other)
        {
            if (other == null)
            {
                throw new ArgumentNullException();// todo add args
            }
            return Id.CompareTo(other);
        }
    }
}
