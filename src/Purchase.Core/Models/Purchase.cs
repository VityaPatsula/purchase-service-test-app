﻿using Purchase.Abstractions.Data;

namespace Purchase.Core.Models
{
    public class Purchase : IEntity<Guid>
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string DisplayName { get; set; }
        public string LastName { get; set; }
        public string PrimaryEmail { get; set; }
        public string SecondaryEmail { get; set; }
        public List<string> OrdersIds { get; set; } // contains id for every order

        public int CompareTo(Guid other)
        {
            return Id.CompareTo(other);
        }
    }
}
