﻿using MediatR;
using Purchase.Abstractions.Data;
using Purchase.Infrastructure.Mediator.Models.Requests;
using Purchase.Infrastructure.Mediator.Models.Responses;

namespace Purchase.Infrastructure.Mediator.Handlers
{
    public class TryGetPurchaseByIdHandler : IRequestHandler<TryGetPurchaseByIdRequest, TryGetPurchaseByIdResponse>
    {
        private readonly IRepository<Core.Models.Purchase, Guid> _purchases;
        public TryGetPurchaseByIdHandler(IRepository<Core.Models.Purchase, Guid> purchases)
        {
            _purchases = purchases;
        }
        public Task<TryGetPurchaseByIdResponse> Handle(TryGetPurchaseByIdRequest request, CancellationToken cancellationToken)
        {
            var success = _purchases.TrySearch(request.Id, out var purchase);
            var result = new TryGetPurchaseByIdResponse(success, purchase);
            return Task.FromResult(result);
        }
    }
}
