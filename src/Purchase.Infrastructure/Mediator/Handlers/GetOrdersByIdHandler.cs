﻿using MediatR;
using Purchase.Abstractions.Data;
using Purchase.Core.Models;
using Purchase.Infrastructure.Exceptions;
using Purchase.Infrastructure.Mediator.Models.Requests;
using Purchase.Infrastructure.Mediator.Models.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Purchase.Infrastructure.Mediator.Handlers
{
    public class GetOrdersByIdHandler : IRequestHandler<GetOrderByIdRequest, GetOrdersResponse>
    {
        private readonly IRepository<Order, string> _orders;
        public GetOrdersByIdHandler(IRepository<Order, string> orders)
        {
            _orders = orders;
        }
        public Task<GetOrdersResponse> Handle(GetOrderByIdRequest request, CancellationToken cancellationToken)
        {
            var orders = new List<Order>();
            foreach(var id in request.OrdersIds)
            {
                var found = _orders.TrySearch(id, out var order);
                if (!found)
                {
                    throw new OrderNotFoundException(id);
                }
                orders.Add(order);
            }
            return Task.FromResult(new GetOrdersResponse(orders));
        }
    }
}
