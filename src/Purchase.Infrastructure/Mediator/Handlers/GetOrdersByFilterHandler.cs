﻿using MediatR;
using Purchase.Abstractions.Data;
using Purchase.Core.Models;
using Purchase.Infrastructure.Mediator.Models.Requests;
using Purchase.Infrastructure.Mediator.Models.Responses;

namespace Purchase.Infrastructure.Mediator.Handlers
{
    public class GetOrdersByFilterHandler : IRequestHandler<GetOrdersByFilterRequest, GetOrdersResponse>
    {
        private readonly IRepository<Order, string> _orders;
        public GetOrdersByFilterHandler(IRepository<Order, string> orders)
        {
            _orders = orders;
        }
        public Task<GetOrdersResponse> Handle(GetOrdersByFilterRequest request, CancellationToken cancellationToken)
        {
            var orders = _orders.Filter(CreateFilter(request), request.Skip, request.Take).ToList();
            return Task.FromResult(new GetOrdersResponse(orders));
        }

        private Predicate<Order> CreateFilter(GetOrdersByFilterRequest request)
        {
            return new Predicate<Order>((order) =>
            {
                return (!request.CreationDate.HasValue || order.CreationDate.Equals(request.CreationDate)) &&
                    (!request.Amount.HasValue || order.Amount.Equals(request.Amount)) &&
                    (string.IsNullOrEmpty(request.Currency) || order.Currency.Contains(request.Currency));
            });
        }
    }
}
