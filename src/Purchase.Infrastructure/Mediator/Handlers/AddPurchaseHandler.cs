﻿using MediatR;
using Purchase.Abstractions.Data;
using Purchase.Core.Models;
using Purchase.Infrastructure.Mediator.Models.Requests;
using Purchase.Infrastructure.Mediator.Models.Responses;

namespace Purchase.Infrastructure.Mediator.Handlers
{
    public class AddPurchaseHandler : IRequestHandler<AddPurchaseRequest, AddPurchaseResponse>
    {
        private readonly IRepository<Core.Models.Purchase, Guid> _purchases;
        private readonly IRepository<Order, string> _orders;
        public AddPurchaseHandler(IRepository<Core.Models.Purchase, Guid> purchases, IRepository<Order, string> orders)
        {
            _purchases = purchases;
            _orders = orders;
        }
        public Task<AddPurchaseResponse> Handle(AddPurchaseRequest request, CancellationToken cancellationToken)
        {
            // map dto to internal classes
            // add Id to models
            var purchase = SetUpPurchase(request);
            var orders = SetUpOrders(request.Orders).ToList();
            purchase.OrdersIds = orders.Select(o => o.Id).ToList();

            // add to repos
            _purchases.Add(purchase);
            foreach(var order in orders)
            {
                _orders.Add(order);
            }
            return Task.FromResult(new AddPurchaseResponse(purchase.Id));
        }

        private Core.Models.Purchase SetUpPurchase(AddPurchaseRequest request)
        {
            return new Core.Models.Purchase
            {
                Id = Guid.NewGuid(),
                DisplayName = request.DisplayName,
                FirstName = request.DisplayName,
                LastName = request.LastName,
                PrimaryEmail = request.PrimaryEmail,
                SecondaryEmail = request.SecondaryEmail
            };
        }

        private IEnumerable<Order> SetUpOrders(IEnumerable<OrderRequest> orders)
        {
            return orders.Select(order => new Order
            {
                Id = Guid.NewGuid().ToString(),
                CreationDate = DateTime.UtcNow,
                Amount = order.Amount,
                Currency = order.Currency
            });
        }
    }
}
