﻿using MediatR;
using Purchase.Abstractions.Data;
using Purchase.Infrastructure.Mediator.Models.Requests;
using Purchase.Infrastructure.Mediator.Models.Responses;

namespace Purchase.Infrastructure.Mediator.Handlers
{
    public class GetPurchaseByFilterHandler : IRequestHandler<GetPurchaseByFilterRequest, GetPurchaseByFilterResponse>
    {
        private readonly IRepository<Core.Models.Purchase, Guid> _purchases;
        public GetPurchaseByFilterHandler(IRepository<Core.Models.Purchase, Guid> purchases)
        {
            _purchases = purchases;
        }
        public Task<GetPurchaseByFilterResponse> Handle(GetPurchaseByFilterRequest request, CancellationToken cancellationToken)
        {
            var result = _purchases.Filter(CreateFilter(request), request.Skip, request.Take).ToList();
            return Task.FromResult(new GetPurchaseByFilterResponse(result));
        }

        private Predicate<Core.Models.Purchase> CreateFilter(GetPurchaseByFilterRequest request)
        {
            return new Predicate<Core.Models.Purchase>((purchase) =>
            {
                return (string.IsNullOrEmpty(request.FirstName) || purchase.FirstName.Contains(request.FirstName)) &&
                (string.IsNullOrEmpty(request.LastName) || purchase.LastName.Contains(request.LastName)) &&
                (string.IsNullOrEmpty(request.DisplayName) || purchase.DisplayName.Contains(request.DisplayName)) &&
                (string.IsNullOrEmpty(request.PrimaryEmail) || purchase.PrimaryEmail.Contains(request.PrimaryEmail)) &&
                (string.IsNullOrEmpty(request.SecondaryEmail) || purchase.SecondaryEmail.Contains(request.SecondaryEmail));
            });
        }
    }
}
