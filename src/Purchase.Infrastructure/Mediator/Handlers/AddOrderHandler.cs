﻿using MediatR;
using Purchase.Abstractions.Data;
using Purchase.Core.Models;
using Purchase.Infrastructure.Exceptions;
using Purchase.Infrastructure.Mediator.Models.Requests;
using Purchase.Infrastructure.Mediator.Models.Responses;

namespace Purchase.Infrastructure.Mediator.Handlers
{
    public class AddOrderHandler : IRequestHandler<AddOrderRequest, AddOrderResponse>
    {
        private readonly IRepository<Core.Models.Purchase, Guid> _purchases;
        private readonly IRepository<Order, string> _orders;
        public AddOrderHandler(IRepository<Core.Models.Purchase, Guid> purchases, IRepository<Order, string> orders)
        {
            _purchases = purchases;
            _orders = orders;
        }
        public Task<AddOrderResponse> Handle(AddOrderRequest request, CancellationToken cancellationToken)
        {
            if(!_purchases.TrySearch(request.PurchaseId, out var purchase))
            {
                throw new PurchaseNotFoundException(request.PurchaseId);
            }

            var orders = SetUpOrders(request.Orders).ToList();
            purchase.OrdersIds.AddRange(orders.Select(o => o.Id));

            foreach(var order in orders)
            {
                _orders.Add(order);
            }
            return Task.FromResult(new AddOrderResponse(orders));
        }

        private IEnumerable<Order> SetUpOrders(IEnumerable<OrderRequest> orders)
        {
            return orders.Select(order => new Order
            {
                Id = Guid.NewGuid().ToString(),
                CreationDate = DateTime.Now,
                Amount = order.Amount,
                Currency = order.Currency
            });
        }
    }
}
