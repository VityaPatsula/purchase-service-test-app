﻿using MediatR;
using Purchase.Infrastructure.Mediator.Models.Responses;

namespace Purchase.Infrastructure.Mediator.Models.Requests
{
    public class AddOrderRequest : IRequest<AddOrderResponse>
    {
        public AddOrderRequest(Guid purchaseId, IEnumerable<OrderRequest> orders)
        {
            PurchaseId = purchaseId;
            Orders = orders;
        }

        public Guid PurchaseId { get; }
        public IEnumerable<OrderRequest> Orders { get; }
    }
}
