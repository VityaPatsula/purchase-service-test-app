﻿using MediatR;
using Purchase.Infrastructure.Mediator.Models.Responses;

namespace Purchase.Infrastructure.Mediator.Models.Requests
{
    public class GetPurchaseByFilterRequest : IRequest<GetPurchaseByFilterResponse>
    {
        public GetPurchaseByFilterRequest(int skip, int take, string? firstName, string? displayName, string? lastName, string? primaryEmail, string? secondaryEmail)
        {
            Skip = skip;
            Take = take;
            FirstName = firstName;
            DisplayName = displayName;
            LastName = lastName;
            PrimaryEmail = primaryEmail;
            SecondaryEmail = secondaryEmail;
        }

        public int Skip { get; }
        public int Take { get; }
        public string? FirstName { get; }
        public string? DisplayName { get; }
        public string? LastName { get; }
        public string? PrimaryEmail { get; }
        public string? SecondaryEmail { get; }
    }
}
