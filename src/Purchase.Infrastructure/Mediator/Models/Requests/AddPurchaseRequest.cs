﻿using MediatR;
using Purchase.Infrastructure.Mediator.Models.Responses;

namespace Purchase.Infrastructure.Mediator.Models.Requests
{
    public class AddPurchaseRequest : IRequest<AddPurchaseResponse>
    {
        public AddPurchaseRequest(string firstName, string displayName, string lastName, string primaryEmail, string secondaryEmail, IEnumerable<OrderRequest> orders)
        {
            FirstName = firstName;
            DisplayName = displayName;
            LastName = lastName;
            PrimaryEmail = primaryEmail;
            SecondaryEmail = secondaryEmail;
            Orders = orders;
        }

        public string FirstName { get; }
        public string DisplayName { get; }
        public string LastName { get; }
        public string PrimaryEmail { get; }
        public string SecondaryEmail { get; }
        public IEnumerable<OrderRequest> Orders { get; }
    }
}
