﻿using MediatR;
using Purchase.Infrastructure.Mediator.Models.Responses;

namespace Purchase.Infrastructure.Mediator.Models.Requests
{
    public class GetOrderByIdRequest : IRequest<GetOrdersResponse>
    {
        public GetOrderByIdRequest(IEnumerable<string> ordersIds)
        {
            OrdersIds = ordersIds;
        }

        public IEnumerable<string> OrdersIds { get; }
    }
}
