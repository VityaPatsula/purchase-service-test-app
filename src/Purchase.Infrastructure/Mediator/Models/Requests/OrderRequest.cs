﻿namespace Purchase.Infrastructure.Mediator.Models.Requests
{
    public class OrderRequest
    {
        public OrderRequest(string currency, int amount)
        {
            Currency = currency;
            Amount = amount;
        }

        public string Currency { get; }
        public int Amount { get; }
    }
}
