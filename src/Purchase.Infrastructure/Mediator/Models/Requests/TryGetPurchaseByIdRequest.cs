﻿using MediatR;
using Purchase.Infrastructure.Mediator.Models.Responses;

namespace Purchase.Infrastructure.Mediator.Models.Requests
{
    public class TryGetPurchaseByIdRequest : IRequest<TryGetPurchaseByIdResponse>
    {
        public TryGetPurchaseByIdRequest(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
    }
}
