﻿using MediatR;
using Purchase.Infrastructure.Mediator.Models.Responses;

namespace Purchase.Infrastructure.Mediator.Models.Requests
{
    public class GetOrdersByFilterRequest : IRequest<GetOrdersResponse>
    {
        public GetOrdersByFilterRequest(int skip, int take, DateTime? creationDate, string? currency, int? amount)
        {
            Skip = skip;
            Take = take;
            CreationDate = creationDate;
            Currency = currency;
            Amount = amount;
        }

        public int Skip { get; }
        public int Take { get; }
        public DateTime? CreationDate { get; } // UTC
        public string? Currency { get; }
        public int? Amount { get; }
    }
}
