﻿namespace Purchase.Infrastructure.Mediator.Models.Responses
{
    public class GetPurchaseByFilterResponse
    {
        public GetPurchaseByFilterResponse(IEnumerable<Core.Models.Purchase> purchase)
        {
            Purchase = purchase;
        }

        public IEnumerable<Core.Models.Purchase> Purchase { get; }
    }
}
