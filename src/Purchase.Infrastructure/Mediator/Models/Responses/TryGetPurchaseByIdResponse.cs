﻿namespace Purchase.Infrastructure.Mediator.Models.Responses
{
    public class TryGetPurchaseByIdResponse
    {
        public TryGetPurchaseByIdResponse(bool success, Core.Models.Purchase purchase)
        {
            Purchase = purchase;
            Success = success;
        }

        public bool Success { get; }
        public Core.Models.Purchase Purchase { get; }
    }
}
