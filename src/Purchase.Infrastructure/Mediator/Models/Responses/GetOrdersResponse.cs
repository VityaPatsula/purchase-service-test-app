﻿using Purchase.Core.Models;

namespace Purchase.Infrastructure.Mediator.Models.Responses
{
    public class GetOrdersResponse
    {
        public GetOrdersResponse(IEnumerable<Order> orders)
        {
            Orders = orders;
        }

        public IEnumerable<Order> Orders { get; }
    }
}
