﻿namespace Purchase.Infrastructure.Mediator.Models.Responses
{
    public class AddPurchaseResponse
    {
        public AddPurchaseResponse(Guid purchaseId)
        {
            PurchaseId = purchaseId;
        }

        public Guid PurchaseId { get; }
    }
}
