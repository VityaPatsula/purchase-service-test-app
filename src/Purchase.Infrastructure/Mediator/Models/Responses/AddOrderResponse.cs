﻿using Purchase.Core.Models;

namespace Purchase.Infrastructure.Mediator.Models.Responses
{
    public class AddOrderResponse
    {
        public AddOrderResponse(IEnumerable<Order> orders)
        {
            Orders = orders;
        }

        public IEnumerable<Order> Orders { get; }
    }
}
