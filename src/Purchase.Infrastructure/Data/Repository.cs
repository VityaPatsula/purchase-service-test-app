﻿using Purchase.Abstractions.Data;
using Purchase.Core.Tree;

namespace Purchase.Infrastructure.Data
{
    public class Repository<T, K> : IRepository<T, K> where T : IEntity<K>
    {
        private readonly BinaryTree<T, K> _tree;
        public Repository(BinaryTree<T, K> tree)
        {
            _tree = tree;
        }
        public void Add(T item)
        {
            _tree.Insert(item);
        }

        public IEnumerable<T> Filter(Predicate<T> filter, int skip = 0, int take = 100)
        {
            return _tree.Filter(filter, skip, take);
        }

        public IEnumerable<T> GetAll(int skip = 0, int take = 100)
        {
            Predicate<T> getAll = (key) => true;
            return _tree.Filter(getAll, skip, take);
        }

        public bool TrySearch(K id, out T result)
        {
            return _tree.TrySearch(id, out result);
        }
    }
}
