﻿namespace Purchase.Infrastructure.Exceptions
{
    public class OrderNotFoundException : Exception
    {
        private readonly string _message;
        public OrderNotFoundException(string id, string? message = null, Exception? innerException = null) : base(message, innerException)
        {
            if (string.IsNullOrEmpty(message))
            {
                message = $"Attempt to find Order with Id = {id} failed";
            }
            _message = message;
        }

        public override string Message
        {
            get { return _message; }
        }
    }
}
