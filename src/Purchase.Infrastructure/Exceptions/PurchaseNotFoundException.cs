﻿namespace Purchase.Infrastructure.Exceptions
{
    public class PurchaseNotFoundException : Exception
    {
        private readonly string _message;
        public PurchaseNotFoundException(Guid PurchaseId, string? message = null, Exception? innerException = null): base(message, innerException)
        {
            if (string.IsNullOrEmpty(message))
            {
                message = $"Attempt to find Purchase with Id = {PurchaseId} failed";
            }
            _message = message;
        }

        public override string Message
        {
            get { return _message; }
        }
    }
}
