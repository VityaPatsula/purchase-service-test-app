﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace Purchase.Infrastructure.Extensions
{
    public static class ConfigMediatR
    {
        public static IServiceCollection AddMediatorLibrary(this IServiceCollection services)
        {
            // TODO: configure manually
            services.AddMediatR(System.Reflection.Assembly.GetExecutingAssembly());
            return services;
        }
    }
}
